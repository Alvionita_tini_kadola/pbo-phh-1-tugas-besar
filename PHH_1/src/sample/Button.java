package sample;

// This class acts as a model class,holding getters,setters and properties

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class Button {

    private final StringProperty  province;


    //Default constructor
    public Button(String province) {
        this.province = new SimpleStringProperty(province);

    }

    //Getters
    public String getprovince() {
        return province.get();
    }



    //Setters
    public void setprovince(String value) {
        province.set(value);
    }



    //Property values
    public StringProperty provinceProperty() {
        return province;
    }


    }


