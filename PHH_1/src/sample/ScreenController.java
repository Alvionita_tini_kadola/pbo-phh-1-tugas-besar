package sample;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.Deque;
import java.util.ResourceBundle;

//import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Node;


public class ScreenController implements Initializable {



    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Button aboutButton;

    @FXML
    private Button exitButton;

    @FXML
    private ComboBox<Button> ProvinsiComboButton;
    private ObservableList<Button> data;
     private DBConnect dc;

    @Override


    public void initialize(URL location, ResourceBundle resources) {
        try {
            VBox box = FXMLLoader.load(getClass().getResource("DrawerContent.fxml"));
            drawer.setSidePane(box);

            HamburgerBackArrowBasicTransition burgerTask = new HamburgerBackArrowBasicTransition(hamburger);
            burgerTask.setRate(-1);
            hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
                burgerTask.setRate(burgerTask.getRate()* -1);
                burgerTask.play();

                    if (drawer.isShown())
                        drawer.close();
                    else
                        drawer.open();

        });
        } catch (IOException e) {
            e.printStackTrace();
        }

    dc =new DBConnect();
    }

    @FXML

    private void loadDataProvinsi(javafx.event.ActionEvent event) throws SQLException {
        Connection conn = dc.Connect();
        data = FXCollections.observableArrayList();
        // Execute query and store result in a resultset
        ResultSet rs = null;
        try {
            rs = conn.createStatement().executeQuery("SELECT * FROM library");
            while (rs.next()) {
                //get string from db,whichever way
                data.add(new Button(rs.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // ProvinsiComboButton.setButton(new PropertyValueFactory<>("name"));


        ProvinsiComboButton.setItems(null);
        ProvinsiComboButton.setItems(data);

    }
    public void aboutShow(ActionEvent event)throws IOException{
            Parent aboutScreenParent= null;
            try {
                aboutScreenParent = FXMLLoader.load(getClass().getResource("aboutScreen.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Scene aboutScreenScene = new Scene(aboutScreenParent);

     Stage window = (Stage)((Node)event.getSource()).getScene().getWindow() ;

     window.setScene(aboutScreenScene);
     window.show();
    }



}



