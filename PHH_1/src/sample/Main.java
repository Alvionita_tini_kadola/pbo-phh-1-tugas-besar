package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Deque;

public class Main extends Application {

    final ObservableList options=FXCollections.observableArrayList();

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Screen.fxml"));
        stage.setTitle("Let's Travel - PHH 1");
        stage.setScene(new Scene(root, 650, 600));
        stage.show();



    }

    public static void main(String[] args) {
        launch(args);}
}
