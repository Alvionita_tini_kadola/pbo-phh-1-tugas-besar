package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class exitButtonController {

    static  boolean answer;

    public static boolean display(String title, String message) {
        Stage primaryStage = new Stage();
        /**
         * mencegah window lainnya dalam status aktif
         */
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle(title);
        primaryStage.setMinWidth(250);
        primaryStage.setMinHeight(250);
        Label label = new Label();
        label.setText(message);

        Button yesButton = new Button("Ya");
        Button noButton = new Button("TIdak");

        yesButton.setOnAction(e -> {
            answer = true;
            primaryStage.close();
        });

        noButton.setOnAction(e -> {
            answer = false;
            primaryStage.close();
        });

        VBox layout = new VBox(10);
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(label, yesButton, noButton);
        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);
        /**
         * menampilkan window alert dan menunggu interaksi pengguna
         */
        primaryStage.showAndWait();

        return answer;
    }
}
